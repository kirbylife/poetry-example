import re

from datetime import datetime

def is_prime(n):
    return not re.match(r"-?$|^(--+?)\1$", "-"*n)

def primes(a, b):
    return list(filter(is_prime, range(a, b)))

def main():
    print("Calculo de números primos")
    a = int(input("Límite inferior: "))
    b = int(input("Límite superior: "))

    result = primes(a, b)
    print(f"Todos los números primos que hay entre {a} y {b} son:")
    print(", ".join(map(str, result)))

if __name__ == "__main__":
    main()
