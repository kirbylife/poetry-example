from example import __version__
from example.main import is_prime, primes

def test_version():
    assert __version__ == '0.1.0'

def test_2_is_prime():
    assert is_prime(2)

def test_4_is_not_prime():
    assert not is_prime(4)

def test_113_is_prime():
    assert is_prime(113)

def test__minus_1_is_not_prime():
    assert not is_prime(-1)

def test_range_from_10_to_30_of_prime_numbers():
    numbers = primes(10, 30)
    assert numbers == [11, 13, 15, 17, 19, 21, 23, 25, 27, 29]

